package com.example.demo.web;

import com.fasterxml.jackson.annotation.JsonInclude;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.Collection;
import java.util.stream.Collectors;

@RestControllerAdvice
public class GeneralControllerAdvice {

    @ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    ErrorDTO handleGeneralException(Exception exception) {
        return new ErrorDTO(exception.getMessage());
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.PRECONDITION_FAILED)
    ErrorDTO handleMethodArgumentNotValidException(MethodArgumentNotValidException manve) {
        return new ErrorDTO(manve.getBindingResult().getObjectName().concat(" failed"),
                manve.getBindingResult().getAllErrors().stream()
                        .map(o -> "[".concat(o.getCodes()[0]).concat("] ").concat(o.getDefaultMessage()))
                        .collect(Collectors.toList()));
    }
    @ExceptionHandler(ConstraintViolationException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ErrorDTO handleConstraintViolationException(ConstraintViolationException cve) {
        return new ErrorDTO(cve.getMessage(),
                cve.getConstraintViolations().stream()
                        .map(ConstraintViolation::getMessage).collect(Collectors.toList()));
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public class ErrorDTO {
        private String mensaje;
        private Collection<String> detalles;

        public ErrorDTO(String mensaje) {
            this.mensaje = mensaje;
        }

        public ErrorDTO(String mensaje, Collection<String> detalles) {
            this.mensaje = mensaje;
            this.detalles = detalles;
        }

        public String getMensaje() {
            return mensaje;
        }

        public Collection<String> getDetalles() {
            return detalles;
        }
    }
}
