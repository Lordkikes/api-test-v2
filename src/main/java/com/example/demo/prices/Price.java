package com.example.demo.prices;

import lombok.Getter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Entity
@Table(name = "PRICES")
public class Price {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PRICE_LIST", updatable = false)
    private Long id;

    @Column(name = "START_DATE")
    private LocalDateTime startDate;

    @Column(name = "END_DATE")
    private LocalDateTime endDate;

    @Column(name = "PRIORITY", nullable = false)
    private PricePriority priority;

    @Column(name = "PRICE", nullable = false)
    private Double value;

    @Column(name = "CURR", nullable = false, length = 4)
    @Enumerated(EnumType.STRING)
    private Currency curr;

    // referencias externas
    @Column(name = "BRAND_ID", nullable = false, updatable = false)
    private Long brandId;

    @Column(name = "PRODUCT_ID", nullable = false, updatable = false)
    private Long productId;

}
