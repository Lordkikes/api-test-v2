package com.example.demo.prices;

import java.time.LocalDateTime;
import java.util.Optional;

public interface PriceService {

    Optional<Price> getMajorPriceByBrandAndProductAndDateThan(Long brandId, Long productId, LocalDateTime atDate);

}
