package com.example.demo.prices;

public enum PricePriority {
    LOW, MEDIUM, HIGH
}
