package com.example.demo.prices;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

@Getter
@NoArgsConstructor
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public class PriceDTO implements Serializable {

    private Long brandId;

    private Long productId;

    private Long rateId;

    private Double perceivedValuePricing;

    private LocalDateTime startDate;

    private LocalDateTime endDate;

    public static PriceDTO of(Price price) {
        return new PriceDTO(price.getBrandId(), price.getProductId(), price.getId(), price.getValue(), price.getStartDate(), price.getEndDate());
    }
}
