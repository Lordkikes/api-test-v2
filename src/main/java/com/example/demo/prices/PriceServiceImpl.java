package com.example.demo.prices;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class PriceServiceImpl implements PriceService {

    private final PriceRepository priceRepository;

    @Override
    public Optional<Price> getMajorPriceByBrandAndProductAndDateThan(Long brandId, Long productId, LocalDateTime atDate) {
        return priceRepository.findFirstByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(brandId, productId, atDate, atDate);
        //return priceRepository.findMajorPrice(brandId, productId, atDate).stream().findFirst();
    }
}
