package com.example.demo.prices;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Optional;

@Repository
public interface PriceRepository extends CrudRepository<Price, Long> {

    Optional<Price> findFirstByBrandIdAndProductIdAndStartDateLessThanEqualAndEndDateGreaterThanEqualOrderByPriorityDesc(Long brandId, Long productId, LocalDateTime startDate, LocalDateTime endDate);

    @Query("FROM Price p WHERE p.brandId = :brandId AND p.productId = :productId AND :atDate BETWEEN p.startDate AND p.endDate ORDER BY p.priority")
    Collection<Price> findMajorPrice(@Param("brandId") Long brandId, @Param("productId") Long productId, @Param("atDate") LocalDateTime atDate);

}
