package com.example.demo.prices;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;

@RestController
@RequiredArgsConstructor
@RequestMapping("brands/{brandId}/prices")
public class PriceController {

    private final PriceService priceService;

    @SneakyThrows
    @GetMapping(value = "/{productId}")
    public ResponseEntity<PriceDTO> getPriceByProduct(@PathVariable("brandId") Long brandId,
                                                      @PathVariable("productId") Long productId,
                                                      @RequestParam(value = "atDate") @DateTimeFormat(fallbackPatterns = {"yyyy-MM-dd-HH.mm.ss", "yyyy-MM-dd HH:mm:ss"}) LocalDateTime atDate) {
        return ResponseEntity.ok(priceService.getMajorPriceByBrandAndProductAndDateThan(brandId, productId, atDate)
                .map(PriceDTO::of).orElseThrow(Exception::new)
        );
    }
}
