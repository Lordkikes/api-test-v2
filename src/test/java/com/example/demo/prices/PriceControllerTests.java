package com.example.demo.prices;

import org.junit.jupiter.api.Test;
import org.mockito.Mock;

class PriceControllerTests {

    private PriceController priceController;
    @Mock
    private PriceService priceService;

    /** Test 1: petición a las 10:00 del día 14 del producto 35455   para la brand 1 (ZARA) */
    @Test
    void test1() {

    }

    /** Test 2: petición a las 16:00 del día 14 del producto 35455   para la brand 1 (ZARA) */
    @Test
    void test2() {

    }

    /** Test 3: petición a las 21:00 del día 14 del producto 35455   para la brand 1 (ZARA) */
    @Test
    void test3() {

    }

    /** Test 4: petición a las 10:00 del día 15 del producto 35455   para la brand 1 (ZARA) */
    @Test
    void test4() {

    }

    /** Test 5: petición a las 21:00 del día 16 del producto 35455   para la brand 1 (ZARA) */
    @Test
    void test5() {

    }

}
